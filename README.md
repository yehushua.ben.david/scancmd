# ScanCmd Script

## Author Information
Yehushua Ben David \
Email: yehushua.ben.david@gmail.com \
URL: [ScanCmd Gitlab](https://gitlab.com/yehushua.ben.david/scancmd)

The script is without any license other than "As is".

## Introduction

The ScanCmd is a bash script designed to monitor the output of a Linux command, returning an alert when the output changes. The script computes an MD5 hash of the command output and continuously checks for any change in this hash, which would indicate a change in the output.

This script can be useful when you need to monitor the output of certain commands for changes, perhaps as part of a larger system or automation script.

## How to use ScanCmd

The script takes a single argument, which is the command you wish to monitor. This is passed to the script when you call it from the command line, enclosed by double-quotes.

Here is the command syntax:
```
ScanCmd "<command>"
```
The command to monitor is passed as a string argument to the `ScanCmd` script command. If the command itself has arguments, they are included inside the quotes.

For example, if you want to detect any change in the list of processes running in your Linux system, you can use the command `ps aux` like so:

```
ScanCmd "ps aux"
```
The script will run, check the output of `ps aux`, and then enter a loop where it will continue re-checking the output every second. If the output ever changes, the script will break the loop, providing you with a visual alert.

Example usage could be as follows:

```
while :; do ./runThis.sh ; ScanCmd "cat runThis.sh" ; done
```

In this example, `runThis.sh` would be a separate shell script you're working on. After running that script, `ScanCmd` would check the content of `runThis.sh` for any changes.

Note: The script runs indefinitely until a change is detected.

## Conclusion

The ScanCmd bash script can provide a powerful tool for system monitoring and automation scripting purposes. It allows easy monitoring of command outputs, providing a simple way to detect when those outputs change. Please feel free to modify and use this script as per your requirements. Users are advised to use this script at their own risk as it is provided "As is" without any guarantees or warranties.

